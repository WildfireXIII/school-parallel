// ---------------------------------------------------
// File: 4-1.c
// Nathan Martindale
//
// Compile: gcc -g -Wall -o 4-1 4-1.c -lpthread
// Usage: ./4-1 <thread_count> <data_count> <min_meas> <max_meas> <bin_count>
// ---------------------------------------------------

#include "my_rand.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

// global variables
int thread_count;
int data_count;
int min_meas;
int max_meas;
int bin_count;
unsigned int seed = 13;
float* data;

float* bin_maxes;
int* bin_counts;

pthread_t* thread_handles;

void *binify();
int findBin(float value, float* bin_maxes, int bin_count, float min_meas);
void printBins(int* bins, long rank);

int main(int argc, char* argv[])
{
	// get desired number of threads and allocate space for them
	thread_count = strtol(argv[1], NULL, 10);  
	thread_handles = malloc(thread_count*sizeof(pthread_t));

	// generate some random data between 0-5 (as in book problem example)
	data_count = strtol(argv[2], NULL, 10);
	data = malloc(data_count*sizeof(float));
	for (int i = 0; i < data_count; i++) { data[i] = my_drand(&seed)*5; }

	// get other cmdline arguments
	min_meas = strtol(argv[3], NULL, 10); 
	max_meas = strtol(argv[4], NULL, 10); 
	bin_count = strtol(argv[5], NULL, 10);

	// allocate space for our bin descriptor arrays
	bin_maxes = malloc(bin_count*sizeof(float));
	bin_counts = malloc(bin_count*sizeof(int));


	float bin_width = ((float)max_meas - (float)min_meas)/bin_count;

	//initialize the bin_maxes
	for (int b = 0; b < bin_count; b++) { bin_maxes[b] = min_meas + bin_width*(b+1); }
	
	// initialize all bins to 0
	for (int i = 0; i < bin_count; i++) { bin_counts[i] = 0; }

	// start the threads
	for (long thread = 0; thread < thread_count; thread++)
	{
		pthread_create(&thread_handles[thread], NULL, binify, (void*)thread);
	}
	
	// wait for the first spawned thread to finish (it should handle waiting for
	// all the rest)
	pthread_join(thread_handles[0], &bin_counts);

	// print them out! (using -1 to represent master thread)
	printBins(bin_counts, -1);
	return 0;
}

// function for debugging and displaying final answer
void printBins(int* bins, long rank)
{
	for (int i = 0; i < bin_count; i++)
	{
		float bin_low, bin_high;
		if (i == 0) { bin_low = min_meas; }
		else { bin_low = bin_maxes[i-1]; }
		
		bin_high = bin_maxes[i];
		
		printf("[%li].%i :: %f - %f :: %i\n", rank, i, bin_low, bin_high, bins[i]);
	}
}

int findBin(float value, float* bin_maxes, int bin_count, float min_meas)
{
	for (int i = 0; i < bin_count; i++) 
	{
		float bin_low, bin_high;
		if (i == 0) { bin_low = min_meas; }
		else { bin_low = bin_maxes[i-1]; }
		
		bin_high = bin_maxes[i];
		
		if (bin_low <= value && value < bin_high) { return i; } 
	}
	return -1;
}

void *binify(void* rank)
{
	long rank_num = (long)rank;
	
	// make some local bins to store the counts that this particular thread sees
	int* loc_bin_cts;
	loc_bin_cts = malloc(bin_count*sizeof(int));

	int data_start = (data_count / thread_count)*(long)rank;
	int data_end = (data_count / thread_count)*((long)rank + 1);
	if (rank_num == thread_count - 1) { data_end = data_count; }
	
	// initialize all local bins to 0
	for (int i = 0; i < bin_count; i++) { loc_bin_cts[i] = 0; }

	// printBins(loc_bin_cts, rank_num); // DEBUG

	// iterate through this thread's assigned data and increment local bin
	// counts
	for (int i = data_start; i < data_end; i++)
	{
		int bin = findBin(data[i], bin_maxes, bin_count, min_meas);
		//printf("{%li} %i %f\n", rank_num, bin, data[i]); // DEBUG
		loc_bin_cts[bin]++;
	}
	
	// printBins(loc_bin_cts, rank_num); // DEBUG

	// tree sum to aggregate all the things
	int divisor = 2;
	int partner_offset = 1;
	int* partner_bin_cts;
	// iterate based on div/2 - we want an extra iteration in case the number of
	// threads isn't a power of 2, and we might have an extra single node to add
	// at the end
	while ((divisor/2) <= thread_count)  
	{
		// printf("{%li} iteration start\n", rank); // DEBUG
		if (rank_num % divisor == 0)
		{
			int partner = rank_num + partner_offset;
			// printf("{%li} proposed partner: %i\n", rank, partner); // DEBUG
			if (partner < thread_count)
			{
				pthread_join(thread_handles[partner], &partner_bin_cts);
				// printBins(partner_bin_cts, rank_num+100); // DEBUG
				for (int i = 0; i < bin_count; i++) { loc_bin_cts[i] += partner_bin_cts[i]; }
			}
		}
		divisor *= 2;
		partner_offset *= 2;
	}

	return loc_bin_cts;
}
