// ---------------------------------------------------
// File: 4-4.c
// Nathan Martindale
//
// Compile: gcc -g -Wall -o 4-4 4-4.c -lpthread
// Usage: ./4-4 <thread_count>
// ---------------------------------------------------

#include "timer.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

int thread_count;

void *feedMeCPUTime();

int main(int argc, char* argv[])
{
	long thread = 0;
	double time_sum = 0;
	double time_avg = 0;
	pthread_t* thread_handles;

	// get the number of threads to use
	thread_count = strtol(argv[1], NULL, 10);  
	
	// allocate space for threads
	thread_handles = malloc(thread_count*sizeof(pthread_t));

	for (thread = 0; thread < thread_count; thread++)
	{
		double start, finish, elapsed;
		
		GET_TIME(start);
		
		// create and immediately join the thread
		pthread_create(&thread_handles[thread], NULL, feedMeCPUTime, (void*)thread);
		pthread_join(thread_handles[thread], NULL);
		
		GET_TIME(finish);
		elapsed = finish - start;
		time_sum += elapsed;
	}

	// display results
	time_avg = time_sum / thread_count;
	printf("The average thread create/join time was: %f\n", time_avg);

	return 0;
}

void *feedMeCPUTime() { return NULL; }
