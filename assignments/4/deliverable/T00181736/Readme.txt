4.1
=======
Compile: 
	gcc -g -Wall -o 4-1 4-1.c my_rand.c -lpthread

Execute:
	./4-1 <thread_count> <data_count> <min_meas> <max_meas> <bin_count>

	thread_count: the number of threads to use in binning the data
	data_count: the number of random data points to generate (NOTE: seed was set to 13, so data will be the same every run unless this is changed)
	min_meas: the minimum value to measure for the bin with smallest values
	max_meas: the maximum value to measure for the bin with largest values
	bin_count: the number of bins to divide the data counts into

Example:
	./4-1 10 10000 0 5 20

Notes:
	Prints out the count in each bin at the end. The middle numbers of each line indicate the range of that bin, and the last number is the count


4.2
=======
Compile:
	gcc -g -Wall -o 4-2 4-2.c my_rand.c -lpthread

Execute:
	./4-2 <thread_count> <number_of_tosses_total>

	thread_count: the number of threads to use to toss darts
	number_of_tosses_total: the number of darts to throw

Example:
	./4-2 10 1000000


4.4
======= 
Compile:
	gcc -g -Wall -o 4-4 4-4.c -lpthread

Execute:
	./4-4 <thread_count>

	thread_count: the number of threads to create and join

Example:
	./4-4 100
