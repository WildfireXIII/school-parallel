// ---------------------------------------------------
// File: 4-2.c
// Nathan Martindale
//
// Compile: gcc -g -Wall -o 4-2 4-2.c -lpthread
// Usage: ./4-2 <thread_count> <number_of_tosses_total>
// ---------------------------------------------------

#include "my_rand.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

// global variables
int thread_count;
long long int number_of_hits = 0;
long long int number_of_tosses_total;
long long int number_of_tosses_each;
pthread_mutex_t mutex;

unsigned int seed = 13;

void *batchMonteCarlo();

int main(int argc, char* argv[])
{
	long thread = 0;
	pthread_t* thread_handles;

	// get cmdline arguments
	thread_count = strtol(argv[1], NULL, 10);  
	number_of_tosses_total = strtol(argv[2], NULL, 10);  

	// determine how many tosses each individual thread has to make
	number_of_tosses_each = number_of_tosses_total / thread_count;

	// allocate space for the threads
	thread_handles = malloc(thread_count*sizeof(pthread_t));
	
	// make a mutex to gate the number_of_hits
	pthread_mutex_init(&mutex, NULL);
	
	// start the threads
	for (thread = 0; thread < thread_count; thread++)
	{
		pthread_create(&thread_handles[thread], NULL, batchMonteCarlo, (void*)thread);
	}
	
	// join all of the threads and compute the answer
	for (thread = 0; thread < thread_count; thread++) { pthread_join(thread_handles[thread], NULL); }
	double pi_estimate = 4*number_of_hits/((double)number_of_tosses_total);
	printf("Estimate of pi: %f\n", pi_estimate);
	
	pthread_mutex_destroy(&mutex);
	return 0;
}

void *batchMonteCarlo(void* rank)
{
	long long int my_tosses_start = number_of_tosses_each * (long)rank;
	long long int my_tosses_end = my_tosses_start + number_of_tosses_each;
	long long int my_tosses_index;
	long long int local_hits = 0;

	double x, y, distance_squared;
	
	// run the randomness algorithm thingy and add to the local number of hits
	for (my_tosses_index = my_tosses_start; my_tosses_index < my_tosses_end; my_tosses_index++)
	{
		x = (my_drand(&seed) - .5)*2;
		y = (my_drand(&seed) - .5)*2;
		distance_squared = x*x + y*y;
		if (distance_squared <= 1) { local_hits++; }
	}

	// safely add the local count to the global count
	pthread_mutex_lock(&mutex);
	number_of_hits += local_hits;
	pthread_mutex_unlock(&mutex);
	
	return NULL;
}
