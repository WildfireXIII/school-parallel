#!/bin/bash

# compile 
mpicc -g -Wall -o mpi_odd_even ex3.28_mpi_odd_even.c

# mpiexec -n <p> mpi_odd_even <global_n>

# 1 process

# 1m
mpiexec -n 1 mpi_odd_even 1000000

# 2m
mpiexec -n 1 mpi_odd_even 2000000

# 4m
mpiexec -n 1 mpi_odd_even 4000000

# 8m
mpiexec -n 1 mpi_odd_even 8000000

# 16m
mpiexec -n 1 mpi_odd_even 16000000

# 2 processes

# 1m
mpiexec -n 2 mpi_odd_even 1000000

# 2m
mpiexec -n 2 mpi_odd_even 2000000

# 4m
mpiexec -n 2 mpi_odd_even 4000000

# 8m
mpiexec -n 2 mpi_odd_even 8000000

# 16m
mpiexec -n 2 mpi_odd_even 16000000

# 4 processes

# 1m
mpiexec -n 4 mpi_odd_even 1000000

# 2m
mpiexec -n 4 mpi_odd_even 2000000

# 4m
mpiexec -n 4 mpi_odd_even 4000000

# 8m
mpiexec -n 4 mpi_odd_even 8000000

# 16m
mpiexec -n 4 mpi_odd_even 16000000
